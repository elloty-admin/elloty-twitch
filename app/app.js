const app = require('express')();
const routes = require('./routes');
var cors = require('cors')

app.use('/',routes);
app.use(cors({origin: '*'}))

app.listen(8888, () => {
  console.log('twitch app listen on 8888');
});