require('dotenv')
var router = require('express').Router()
var request = require('request');
var twitchApi = require('./twitchApi');
// getStreams
router.get('/', function (req, res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

  request(twitchApi.getStreams,function(err, response, body){
    if (!err && body && !body.error){
      res.status(200).json(JSON.parse(body));
    } else {
      res.status(500).send(err || body)
    }
  })
})
// getStreams
router.get('/:game', function (req, res) {
	res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed

  request(`${twitchApi.getStreamsByGame}&game=${req.params.game}`,function(err, response, body){
    if (!err && body && !body.error){
      res.status(200).json(JSON.parse(body));
    } else {
      res.status(500).send(err || body)
    }
  })
})

module.exports = router;