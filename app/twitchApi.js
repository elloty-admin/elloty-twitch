const {twitchAPI} = require('./config');
module.exports = {
  getStreams: `https://api.twitch.tv/kraken/streams/?client_id=${twitchAPI}`,
  getStreamsByGame: `https://api.twitch.tv/kraken/streams/?client_id=${twitchAPI}`,
}