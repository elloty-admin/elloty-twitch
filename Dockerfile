FROM node:8.6.0

WORKDIR /usr/app

RUN npm install --quiet

COPY . .

